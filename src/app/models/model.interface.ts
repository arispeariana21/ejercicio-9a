export interface TIPO1 {
    id: number;
    name: string;
}

export interface TIPO2 {
    id: number;
    tipo1id: number;
    name: string;
}
export interface TIPO3 {
    id: number;
    tipo2id: number;
    name: string;
}