import { Component, OnInit } from '@angular/core';
import { TIPO1, TIPO2, TIPO3 } from 'src/app/models/model.interface';
import { DataService } from 'src/app/services/data.service';
//import { data } from "module";

@Component({
  selector: 'app-form',
  templateUrl: './form.component.html',
  styleUrls: ['./form.component.css'],
  providers: [DataService]
})
export class FormComponent implements OnInit {

  public selectedTipo1: TIPO1={ id: 0, name: ''};
  public tipo1!: TIPO1[];
  public tipo2!: TIPO2[]; 
  public tipo3!: TIPO3[];

  constructor(private dataSvc: DataService) { }

  ngOnInit(): void {
    this.tipo1 = this.dataSvc.getTIPO1S();
    
  }

  onSelect(id: number): void{
    this.tipo2 = this.dataSvc.getTIPO2S().filter(item => item.tipo1id == id);
    
  }

  onSelect2(id: number): void{
    this.tipo3 = this.dataSvc.getTIPO3S().filter(item => item.tipo2id == id);
  }
}
