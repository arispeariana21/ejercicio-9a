import { Injectable } from '@angular/core';
import { TIPO1, TIPO2, TIPO3} from '../models/model.interface';

@Injectable()

export class DataService {

  private tipo1: TIPO1[] = [
    {
      id: 0,
      name: 'SELECIONE EL TIPO DE COMIDA QUE DESEA ORDENAR'
    },
    {
      id: 1,
      name: 'Sopa'
    },
    {
      id: 2,
      name: 'Chatarra'
    },
    {
      id: 3,
      name: 'Postre'
    },
    {
      id: 4,
      name: 'Segundo'
    }
  ];

  private tipo2: TIPO2[] = [
    {
      id: 1,
      tipo1id: 1,
      name: 'Pollo'
    },
    {
      id: 2,
      tipo1id: 1,
      name: 'Verduras'
    },
    {
      id: 3,
      tipo1id: 1,
      name: 'Fideo'
    },
    {
      id: 4,
      tipo1id: 1,
      name: 'Arroz'
    },
    {
      id: 5,
      tipo1id: 2,
      name: 'Hamburguesa'
    },
    {
      id: 6,
      tipo1id: 2,
      name: 'Pizza'
    },
    {
      id: 7,
      tipo1id: 2,
      name: 'Salchipapa'
    },
    {
      id: 8,
      tipo1id: 3,
      name: 'Chantillin'
    },
    {
      id: 9,
      tipo1id: 3,
      name: 'Fruta'
    },
    {
      id: 10,
      tipo1id: 3,
      name: 'Gelatina'
    },
    {
      id: 11,
      tipo1id: 3,
      name: 'Budín'
    },
    {
      id: 12,
      tipo1id: 3,
      name: 'Pastel'
    },
    {
      id: 13,
      tipo1id: 3,
      name: 'Cupcake'
    },
    {
      id: 14,
      tipo1id: 4,
      name: 'Sajta de Pollo'
    },
    {
      id: 15,
      tipo1id: 4,
      name: 'Pique de Macho'
    },
    {
      id: 16,
      tipo1id: 4,
      name: 'Chuleta de Res'
    },
    {
      id: 17,
      tipo1id: 4,
      name: 'Lasaña'
    },
    {
      id: 18,
      tipo1id: 4,
      name: 'Albondigas'
    }
  ];

  private tipo3: TIPO3[] =
    //data: 
    [
    {
      id: 1,
      tipo2id: 1,
      name: 'Pollo' 
    },
    {
      id: 2,
      tipo2id: 1,
      name: 'Arroz' 
    },
    {
      id: 3,
      tipo2id: 1,
      name: 'Papa' 
    },
    {
      id: 4,
      tipo2id: 2,
      name: 'Verduras' 
    },
    {
      id: 5,
      tipo2id: 2,
      name: 'Arroz' 
    },
    {
      id: 6,
      tipo2id: 2,
      name: 'Papa' 
    },
    {
      id: 7,
      tipo2id: 2,
      name: 'Carne Roja' 
    },
    {
      id: 8,
      tipo2id: 3,
      name: 'Fideo' 
    },
    {
      id: 9,
      tipo2id: 3,
      name: 'Papa' 
    },
    {
      id: 10,
      tipo2id: 3,
      name: 'Carne Roja' 
    },
    {
      id: 11,
      tipo2id: 4,
      name: 'Arroz' 
    },
    {
      id: 12,
      tipo2id: 4,
      name: 'Papa' 
    },
    {
      id: 13,
      tipo2id: 4,
      name: 'Carne Roja' 
    },
    {
      id: 14,
      tipo2id: 4,
      name: 'Verduras' 
    },
    {
      id: 15,
      tipo2id: 5,
      name: 'Carne de Res' 
    },
    {
      id: 16,
      tipo2id: 5,
      name: 'Pan' 
    },
    {
      id: 17,
      tipo2id: 5,
      name: 'Tomate' 
    },
    {
      id: 18,
      tipo2id: 5,
      name: 'Cebolla' 
    },
    {
      id: 19,
      tipo2id: 5,
      name: 'Lechuga' 
    },
    {
      id: 20,
      tipo2id: 5,
      name: 'Mayonesa' 
    },
    {
      id: 21,
      tipo2id: 5,
      name: 'Ketchup' 
    },
    {
      id: 22,
      tipo2id: 5,
      name: 'Mostaza' 
    },
    {
      id: 23,
      tipo2id: 6,
      name: 'Tomate'
    },
    {
      id: 24,
      tipo2id: 6,
      name: 'Carne de Res' 
    },
    {
      id: 25,
      tipo2id: 6,
      name: 'Queso' 
    },
    {
      id: 26,
      tipo2id: 7,
      name: 'Salchichas' 
    },
    {
      id: 27,
      tipo2id: 7,
      name: 'Papas' 
    },
    {
      id: 28,
      tipo2id: 7,
      name: 'Mayonesa' 
    },
    {
      id: 29,
      tipo2id: 7,
      name: 'Ketchup' 
    },
    {
      id: 30,
      tipo2id: 7,
      name: 'Mostaza' 
    },
    {
      id: 31,
      tipo2id: 8,
      name: 'Leche' 
    },
    {
      id: 32,
      tipo2id: 8,
      name: 'Platano' 
    },
    {
      id: 33,
      tipo2id: 8,
      name: 'Gelatina' 
    },
    {
      id: 34,
      tipo2id: 9,
      name: 'Platano' 
    },
    {
      id: 35,
      tipo2id: 9,
      name: 'Naranja' 
    },
    {
      id: 36,
      tipo2id: 9,
      name: 'Mandarina' 
    },
    {
      id: 37,
      tipo2id: 9,
      name: 'Piña' 
    },
    {
      id: 38,
      tipo2id: 9,
      name: 'Frutilla' 
    },
    {
      id: 39,
      tipo2id: 10,
      name: 'Sabor Frutilla' 
    },
    {
      id: 40,
      tipo2id: 10,
      name: 'Sabor Naranja' 
    },
    {
      id: 41,
      tipo2id: 10,
      name: 'Sabor Menta' 
    },
    {
      id: 42,
      tipo2id: 11,
      name: 'Chocolate'
    },
    {
      id: 43,
      tipo2id: 11,
      name: 'Leche' 
    },
    {
      id: 44,
      tipo2id: 11,
      name: 'Queso' 
    },
    {
      id: 45,
      tipo2id: 12,
      name: 'Crema' 
    },
    {
      id: 46,
      tipo2id: 12,
      name: 'Chocolate' 
    },
    {
      id: 47,
      tipo2id: 12,
      name: 'Frutilla' 
    },
    {
      id: 48,
      tipo2id: 12,
      name: 'Durazno' 
    },
    {
      id: 49,
      tipo2id: 12,
      name: 'Helado' 
    },
    {
      id: 50,
      tipo2id: 13,
      name: 'Chocolate' 
    },
    {
      id: 51,
      tipo2id: 13,
      name: 'Frambuesa' 
    },
    {
      id: 52,
      tipo2id: 13,
      name: 'Zarzamora' 
    },
    {
      id: 53,
      tipo2id: 13,
      name: 'Cereza' 
    },
    {
      id: 54,
      tipo2id: 13,
      name: 'Platano' 
    },
    {
      id: 55,
      tipo2id: 14,
      name: 'Pollo' 
    },
    {
      id: 56,
      tipo2id: 14,
      name: 'Papa Cocida' 
    },
    {
      id: 57,
      tipo2id: 14,
      name: 'Tunta' 
    },
    {
      id: 58,
      tipo2id: 14,
      name: 'Tomate' 
    },
    {
      id: 59,
      tipo2id: 14,
      name: 'Cebolla' 
    },
    {
      id: 60,
      tipo2id: 14,
      name: 'Arroz' 
    },
    {
      id: 61,
      tipo2id: 15,
      name: 'Carne de Res' 
    },
    {
      id: 62,
      tipo2id: 15,
      name: 'Papas Fritas' 
    },
    {
      id: 63,
      tipo2id: 15,
      name: 'Huevo Cocido' 
    },
    {
      id: 64,
      tipo2id: 15,
      name: 'Tomate' 
    },
    {
      id: 65,
      tipo2id: 15,
      name: 'Cebolla' 
    },
    {
      id: 66,
      tipo2id: 15,
      name: 'Morrón' 
    },
    {
      id: 67,
      tipo2id: 15,
      name: 'Salchicha' 
    },
    {
      id: 68,
      tipo2id: 16,
      name: 'Chuleta de Res' 
    },
    {
      id: 69,
      tipo2id: 16,
      name: 'Arroz' 
    },
    {
      id: 70,
      tipo2id: 16,
      name: 'Papa Cocida' 
    },
    {
      id: 71,
      tipo2id: 16,
      name: 'Ensalada' 
    },
    {
      id: 72,
      tipo2id: 17,
      name: 'Carne de Res' 
    },
    {
      id: 73,
      tipo2id: 17,
      name: 'Tomate' 
    },
    {
      id: 74,
      tipo2id: 17,
      name: 'Cebolla' 
    },
    {
      id: 75,
      tipo2id: 17,
      name: 'Queso' 
    },
    {
      id: 76,
      tipo2id: 18,
      name: 'Carne de Res' 
    },
    {
      id: 77,
      tipo2id: 18,
      name: 'Tallarín' 
    },
    {
      id: 78,
      tipo2id: 18,
      name: 'Queso' 
    },
    {
      id: 79,
      tipo2id: 18,
      name: 'Papa en Rodajas' 
    }
  ];

  getTIPO1S(): TIPO1[] {
    return this.tipo1;
  }
  getTIPO2S(): TIPO2[] {
    return this.tipo2;
  }
  getTIPO3S(): TIPO3[] {
    return this.tipo3;
  }
}